/*
 In myFirstDB, do the following:

 	1. Create a "courses" collection and add the following three courses in ONE command:
 		- Basic HTML
 		- Basic CSS
 		- Basic JavaScript
 	2. In the same command, make sure to specify the course's description, price, isActive, and add an enrollees array.

 	For Basic JavaScript ONLY, include an object inside the enrollees array. The object must include a userId field with a valie of our only user's ObjectID.

 */

 db.courses.insertMany([
 	{
 		course: "Basic HTML", 
 		description: "Lorem Ipsum", 
 		price: 10000,
 		enrollees: ["John", "Jane", "Joe"]
 	},
 	{
 		course: "Basic CSS",
  		description: "Lorem Ipsum", 
 		price: 10000,
 		enrollees: ["John", "Jane", "Joe"]
 	},
 	{
 		course: "Basic JavaScript",
  		description: "Lorem Ipsum", 
 		price: 10000,
 		enrollees: [
 			{
 				userId: "61fbcaa4b3b0614cd8c4350d"
 			}
 		]
 	}
 ])